<?php

    Route::prefix('admin')->group(function () {

        Route::get('migrate', function(){
            Artisan::call('migrate:fresh',['--seed' => true]);
            return 'done';
        });

        $models = config('admin.model');

        $repeaters = config('admin.repeater');

        //repeater
        if(count($repeaters)){
            Route::prefix('repeat')->middleware('auth')->group(function() use($repeaters){
                foreach ($repeaters as $module => $repeater){
                    foreach ($repeater as $value){
                        Route::get("{$module}-{$value}", function() use ($module,$value){
                            return view("admin.repeat.{$module}.{$value}");
                        })->name("repeat.{$module}.{$value}");
                    }

                }
            });

        }

//        Route::get('', 'DashboardController@index')->name('admin.dashboard');
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');



        foreach ($models as $model) {
            $modelName = $model['name'];
            $controller = ucfirst($modelName) . "Controller";

            Route::resource($modelName, $controller, ['except' => $model['except']]);
            if (!in_array("bulkDestroy", $model['except'])) {
                Route::delete($modelName, "$controller@bulkDestroy")->name($modelName . ".bulkDestroy");
                Route::put($modelName."/{id}/ajax-display", "$controller@ajaxDisplay")->name($modelName . ".ajaxDisplay");
                Route::put($modelName."/{id}/ajax-feature", "$controller@ajaxFeature")->name($modelName . ".ajaxFeature");
            }
        }
    });

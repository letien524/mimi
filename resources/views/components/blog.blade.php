<?php
$link = route('blog.detail',['slug'=> $blog['slug']]);
$name = $blog['name'];
$date = \Carbon\Carbon::parse($blog['created_at'])->format('Y-m-d');

?>
<div class="col-xs-2 col-md-4 _item">
    <div class="vk-blog-item">
        <a href="{{$link}}" title="{{$name}}" class="vk-blog-item__img">
            <img src="{{imageUrlRender($blog['image'])}}" alt="{{$name}}">
            <span class="vk-blog-item__date">{{\Carbon\Carbon::parse($blog['created_at'])->format('Y-m-d')}}</span>
        </a>
        <div class="vk-blog-item__brief">
            <h3 class="vk-blog-item__name"><a href="{{$link}}" title="{{$name}}">{{$name}}</a></h3>
            <div class="vk-blog-item__content">{{$blog['excerpt']}}</div>
        </div>
    </div>
</div>



<?php
    $link = route('product.detail',$product['slug']);
    $name = $product['name'];
    $image = imageUrlRender($product['image']);


?>
<div class="col-xs-6 col-md-4 col-lg-3 _item">
    <div class="vk-product-item">
        <a href="{{$link}}" class="vk-product-item__img" title="{{$name}}">
            <img src="{{$image}}" alt="{{$name}}">
        </a>

        <div class="vk-product-item__brief">
            <h3 class="vk-product-item__name"><a href="{{$link}}" title="{{$name}}">{{$name}}</a></h3>
            <div class="vk-product-item__price">Liên hệ</div>
        </div>
    </div>
</div>

<?php


$link = route('event.detail',['slug'=> $event[getColumnByLang('slug')]]);
$name = $event[getColumnByLang('name')];

?>
@if(@$eventStyle == 'feature')
    <div class="l_news_item">
        <a href="{{ $link }}" class="l_img_news">
            <img src="{{ imageUrlRender( $event['image']) }}">
        </a>
        <h4><a href="{{ $link }}">{{ $name }}</a></h4>
    </div>

@elseif(@$eventStyle == 'related')
    <div class="event_item">
        <a href="{{$link}}" class="img_event">
            <img src="{{ imageUrlRender( $event['image']) }}">
        </a>
        <h4>
            <a href="{{$link}}">{{ $name }}</a>
        </h4>
        <span class="date_event">{{\Carbon\Carbon::parse($event['created_at'])->format('d/m/Y')}}</span>
    </div>
@else
    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="event_item">
            <a href="{{$link}}" class="img_event">
                <img src="{{ imageUrlRender( $event['image']) }}">
            </a>
            <h4>
                <a href="{{$link}}">{{ $name }}</a>
            </h4>
            <span class="date_event">{{\Carbon\Carbon::parse($event['created_at'])->format('d/m/Y')}}</span>
        </div>
    </div>
@endif

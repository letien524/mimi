@extends('index')

@section('content')

    @include('components.banner-style',['banner'=>@$about['banner']])

    <div class="visible-md-block visible-lg-block"></div>
    <div class="main full_container">
        <div class="container">

            @include('section.header')

            <div class="container contents_warp">

            <div class="contents_side">

        <div class="main_contents">
            <div class="product">
                @if(count(@$home['content']['category']))
                    @foreach($home['content']['category'] as $cat)
                        @if(strlen(@$cat['name']))
                            <a href="{{@$cat['link']}}" title="{{$cat['name']}}">{{$cat['name']}}</a>
                        @endif
                    @endforeach
                @endif
            </div>


            <h1 class="sr-only">{{$metaTitle}}</h1>

            <div class="notice">
                <a class="vk-home-blog__title" href="{{route('blog')}}" title="{{__('Tin tức')}}"><strong>{{__('Tin tức')}}</strong></a>
                <ul>
                    @foreach($blogs as $blog)
                        <li class="post">
                            <div class="title">
                                <a href='{{route('blog.detail',$blog['slug'])}}' title="{{$blog['name']}}">{{$blog['name']}}</a>
                            </div>
                            <div class="excerpt">
                                <a href='{{route('blog.detail',$blog['slug'])}}' title="{{$blog['name']}}">{{$blog['excerpt']}}</a>
                            </div>
                            <div class="date">
                                <a href='{{route('blog.detail',$blog['slug'])}}'>{{\Carbon\Carbon::parse($blog['created_at'])->format('Y-m-d')}}</a>
                            </div>
                            <a class="prev_btn" href="javascript:NoticePrevNext('prev',{{$loop->count - 1}},{{$loop->index}});void(0);">
                                <img src="{{getAsset('images/main/notice_btn_up.jpg')}}" border="0" alt="prev"/>
                            </a>
                            <a class="next_btn" href="javascript:NoticePrevNext('next',{{$loop->count - 1}},{{$loop->index}});void(0);">
                                <img src="{{getAsset('images/main/notice_btn_down.jpg')}}" border="0" alt="next"/>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="bottom_conetent">
                <a href="{{route('contact')}}" class="vk-home__cat" title="{{__('Liên hệ với chúng tôi')}}">
                    <img src="{{getAsset('')}}/images/main/main_contact.jpg" border="0"/>
                    <span class="vk-home__cate-label">{{__('Liên hệ với chúng tôi')}}</span>
                </a>
                <a href="{{route('blog')}}" class="vk-home__cat" title="{{__('Tin tức thị trường')}}">
                    <img src="{{getAsset('')}}/images/main/main_money.jpg" border="0"/>
                    <span class="vk-home__cate-label">{{__('Tin tức thị trường')}}</span>
                </a>
            </div>
        </div>
    </div>

            </div>
        </div> {{--./container--}}
    </div>
@endsection
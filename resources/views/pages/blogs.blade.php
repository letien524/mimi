@extends('index')
@section('content')
    @include('components.banner-style',['banner'=>@$page['banner']])

    <div class="sub full_container visual01">
        <div class="container">
            @include('section.header')

            @include('components.breadcrumb',['breadcrumb'=> $breadcrumb])

            <div class="container contents_warp">
                <div class="contents_side">
                    <div class="sub_contents design map">
                        <div class="location02">
                            <h1 class="title">{{__('Tin tức')}}</h1>
                        </div>

                        @if(count($blogs))
                            <div class="row vk-blog__row">
                                @foreach($blogs as $blog)
                                    @include('components.blog')
                                @endforeach
                            </div>

                            {!! $blogs->links() !!}
                        @endif
                    </div>
                </div>
            </div>
            @include('components.go-to-top')
        </div>
    </div>
@endsection

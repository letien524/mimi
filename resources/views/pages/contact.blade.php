@extends('index')

@section('content')
    @include('components.banner-style',['banner'=>@$page['banner']])

    <div class="sub full_container visual01">
        <div class="container">
            @include('section.header')

            @include('components.breadcrumb',['breadcrumb'=> $breadcrumb])

            <div class="container contents_warp">
                <div class="contents_side">
                    <div class="sub_contents design map">
                        <div class="location02">
                            <h1 class="sr-only">{{$metaTitle}}</h1>
                            <h2 class="title">{{__('Liên hệ')}}</h2>
                        </div>
                        <div class="vk-contact__map">
                            {!! @$page['content']['map'] !!}
                        </div>
                        <hr />

                        <div class="row">
                            <div class="col-lg-6">
                                <ul>
                                    <li>
                                        <em>{{ @$site_settings['company_name'] }} </em>
                                        <br>

                                        {!! @$site_settings['company_address'] !!}
                                        {!! @$site_settings['company_hotline'] !!}
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-6">
                                <ul>
                                    <li>
                                        <em>{{ __('ĐĂNG KÝ NHẬN LIÊN HỆ') }} </em>
                                        <br>

                                        <form action="{{route('contact')}}" method="post">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Họ tên" name="name" value="{{old('name')}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" class="form-control" placeholder="Số điện thoại" name="phone" value="{{old('phone')}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="address" class="form-control" placeholder="Email" name="address" value="{{old('address')}}">
                                            </div>

                                            <div class="form-group">
                                                <textarea name="content" class="form-control" rows="5">{{old('content')}}</textarea>
                                            </div>
                                            <div class="form-group text-center">
                                                <button class="btn btn-default" type="submit">Gửi</button>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <hr />
                    </div>
                </div>
            </div>
            @include('components.go-to-top')
        </div>
    </div>

@endsection
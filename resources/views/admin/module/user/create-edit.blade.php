@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',isUpdate(@$module['action']) ? config('admin.method.edit.display_name') : config('admin.method.create.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{!! updateOrStoreRouteRender(@$module['action'],$module['model'],@$data) !!}" method="post" enctype="multipart/form-data">
    @if(isAdminUrl())
        {{ csrf_field() }}
        @if(isUpdate(@$module['action']))
            {{method_field('put')}}
        @endif
    @endif
    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1"data-toggle="tab">{!! config('admin.tab.info.display_name') !!}</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab-1">
                    <div class="row">
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Tài khoản</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name',@$data['name']) }}" {{ isCurrentRoute('user.edit') ? 'disabled' : null }}>
                            </div>

                            <div class="form-group">
                                <label>Mật khẩu</label>
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}" autocomplete="new-password">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email',@$data['email']) }}">
                            </div>

                            <div class="form-group">
                                <label>Tên chi nhánh</label>
                                <input type="text" class="form-control" name="department_name" value="{{ old('department_name',@$data['department_name']) }}">
                            </div>


                        </div> {{--./col-lg-4--}}

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Họ tên</label>
                                <input type="text" class="form-control" name="display_name" value="{{ old('display_name',@$data['display_name']) }}">
                            </div>

                            <div class="form-group">
                                <label>Ngày sinh</label>
                                <input type="text" class="form-control datepicker" name="date_of_birth" value="{{ old('date_of_birth',@$data['date_of_birth']) }}" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone',@$data['phone']) }}">
                            </div>

                            <div class="form-group">
                                <label>Tỉnh thành</label>
                                <input type="text" class="form-control" name="province" value="{{ old('province',@$data['province']) }}">
                            </div>
                        </div>

                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Cấp bậc</label>
                                <select name="role_id" class="form-control select">
                                    <option value="">Chọn</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" {{ old('role_id', @$data->role->id) ==  $role->id ? 'selected' : ''  }}>{{$role->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Cấp trên</label>
                                <select name="parent_id" class="form-control select">
                                    <option value="">Chọn</option>
                                    @foreach($users as $item)
                                        <option value="{{ $item->id }}" {{ old('parent_id', @$data->parent_id) == $item->id ? 'selected' : ''}}>{{ $item->display_name }} - {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tên ngân hàng</label>
                                <input type="text" class="form-control" name="bank_account_name" value="{{ old('bank_account_name',@$data['bank_account_name']) }}">
                            </div>

                            <div class="form-group">
                                <label>Số tài khoản</label>
                                <input type="text" class="form-control" name="bank_account_number" value="{{ old('bank_account_number',@$data['bank_account_number']) }}">
                            </div>

                            <div class="form-group">
                                <label>Ghi chú</label>
                                <textarea name="note" rows="5" class="form-control">{{ old('note', @$data->note) }}</textarea>
                            </div>

                        </div>



                    </div>
                </div> <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        @if(isAdminUrl())
            <div class="box box-solid">
                <div class="box-body">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
                    @include("admin.components.save-as")
                </div>
            </div>
        @endif
    </form>
@endsection

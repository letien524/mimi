@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',isUpdate(@$module['action']) ? config('admin.method.edit.display_name') : config('admin.method.create.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{!! updateOrStoreRouteRender(@$module['action'],$module['model'],@$data) !!}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if(isUpdate(@$module['action']))
        {{method_field('put')}}
    @endif
    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1"data-toggle="tab">{!! config('admin.tab.general.display_name') !!}</a></li>
                <li class=""><a href="#tab-3"data-toggle="tab">{!! config('admin.tab.seo.display_name') !!}</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Favicon</label>
                                <div class="image">
                                    <?php $value = old('content.favicon',@$data['content']['favicon']); ?>
                                    <div class="image__thumbnail">
                                        <img src="{{ imageUrlRender($value) }}"  data-init="{!! __IMAGE_THUMBNAIL_DEFAULT !!}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" value="{{ $value }}" name="content[favicon]"  />
                                        <div class="image__button" onclick="fileSelect(this)"><i class="fa fa-upload"></i> Upload</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Logo desktop</label>
                                <div class="image">
                                    <?php $value = old('content.logo.desktop',@$data['content']['logo']['desktop']); ?>
                                    <div class="image__thumbnail">
                                        <img src="{{ imageUrlRender($value) }}"  data-init="{!! __IMAGE_THUMBNAIL_DEFAULT !!}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" value="{{ $value }}" name="content[logo][desktop]"  />
                                        <div class="image__button" onclick="fileSelect(this)"><i class="fa fa-upload"></i> Upload</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Logo desktop</label>
                                <div class="image">
                                    <?php $value = old('content.logo.mobile',@$data['content']['logo']['mobile']); ?>
                                    <div class="image__thumbnail">
                                        <img src="{{ imageUrlRender($value) }}"  data-init="{!! __IMAGE_THUMBNAIL_DEFAULT !!}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" value="{{ $value }}" name="content[logo][mobile]"  />
                                        <div class="image__button" onclick="fileSelect(this)"><i class="fa fa-upload"></i> Upload</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Tên công ty</label>
                                <input type="text" name="content[company_name]" class="form-control" value="{{old('content.company_name', @$data['content']['company_name'])}}">
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <textarea name="content[company_address]" class="form-control content" rows="5">{{ old('content.company_address', @$data['content']['company_address']) }}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Hotline</label>
                                <textarea name="content[company_hotline]" class="form-control content" rows="5">{{old('content.company_hotline', @$data['content']['company_hotline']) }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Script đầu trang</label>
                                <textarea name="content[script_head]" class="form-control" rows="5">{!! old('content.script_head', @$data['content']['script_head']) !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Script cuối trang</label>
                                <textarea name="content[script_foot]" class="form-control" rows="5">{!! old('content.script_foot', @$data['content']['script_foot']) !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.tab-pane -->

                <div class="tab-pane " id="tab-3">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta image</label>
                                <div class="image">
                                    <?php $value = old('content.meta_image',@$data['content']['meta_image']); ?>
                                    <div class="image__thumbnail">
                                        <img src="{{ imageUrlRender($value) }}"  data-init="{!! __IMAGE_THUMBNAIL_DEFAULT !!}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" value="{{ $value }}" name="content[meta_image]"  />
                                        <div class="image__button" onclick="fileSelect(this)"><i class="fa fa-upload"></i> Upload</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="content[index]" value="1" {{ old('content.index', @$data['content']['index']) == 1  ? 'checked' : '' }}>
                                        Index
                                    </label>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" name="content[meta_title]" class="form-control" value="{{ old('content.meta_title', @$data['content']['meta_title']) }}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="content[meta_description]" class="form-control" rows="5">{!! old('content.meta_description', @$data['content']['meta_description']) !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title (Tiếng Anh)</label>
                                <input type="text" name="content[meta_title_en]" class="form-control" value="{{ old('content.meta_title_en', @$data['content']['meta_title_en']) }}">
                            </div>
                            <div class="form-group">
                                <label>Meta description (Tiếng Anh)</label>
                                <textarea name="content[meta_description_en]" class="form-control" rows="5">{!! old('content.meta_description_en', @$data['content']['meta_description_en']) !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title (Tiếng Hàn)</label>
                                <input type="text" name="content[meta_title_ko]" class="form-control" value="{{ old('content.meta_title_ko', @$data['content']['meta_title_ko']) }}">
                            </div>
                            <div class="form-group">
                                <label>Meta description (Tiếng Hàn)</label>
                                <textarea name="content[meta_description_ko]" class="form-control" rows="5">{!! old('content.meta_description_ko', @$data['content']['meta_description_ko']) !!}</textarea>
                            </div>
                        </div>

                    </div>
                </div> <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

        <div class="box box-solid">
            <div class="box-body">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
                @include("admin.components.save-as")
            </div>
        </div>
    </form>
@endsection
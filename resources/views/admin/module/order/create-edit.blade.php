@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',isUpdate(@$module['action']) ? config('admin.method.edit.display_name') : config('admin.method.create.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{!! updateOrStoreRouteRender(@$module['action'],$module['model'],@$data) !!}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if(isUpdate(@$module['action']))
        {{method_field('put')}}
    @endif
    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1"data-toggle="tab">{!! config('admin.tab.info.display_name') !!}</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab-1">
                    <div class="row">
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Đại lý</label>
                                <select name="user_id" class="form-control select">
                                    <option value="">Chọn</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}" {{ old('user_id', @$data->user_id) == $user->id ? 'selected' : '' }}>{{$user->display_name}} - {{$user->id}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Chiết khấu: (0 - 100)</label>
                                <input type="number" class="form-control" name="discount" min="0" value="{{ old('discount', @$data->discount) }}" id="discount" onkeyup="orderQuantityOnchange(this, false, '.table')">
                            </div>

                        </div> {{--./col-lg-4--}}
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ghi chú</label>
                                <textarea name="note" rows="5" class="form-control">{{ old('note', @$data->note) }}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="repeater" id="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <th style="width: 30px;">No.</th>
                                    <th>Sản phẩm</th>
                                    <th style="width: 20%">Giá</th>
                                    <th style="width: 20%">Số lượng</th>
                                    <th style="width: 20%">Đơn giá</th>
                                    </thead>

                                    <tbody id="sortable">
                                    <?php
                                        $oldOrderDetails = old('order_detail', @$data->orderDetails ? $data->orderDetails->toArray() : []);

                                    ?>

                                        @foreach($oldOrderDetails as $key => $val)
                                            @include('admin.repeat.order.product',[
                                            'index'=>$loop->iteration,
                                            'id'=>$key,
                                            'val'=>$val,
                                            ])
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="4">Tổng:</th>
                                        <th><span class="price-global">{{ @$data['price_total'] ? number_format($data['price_total'],0,',','.') : 0 }}</span></th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Chiết khấu:</th>
                                        <?php $priceDiscount = @$data['discount'] ? ($data['discount']/100) * $data['price_total'] : 0; ?>
                                        <th><span class="price-discount text-primary">{{  number_format($priceDiscount,0,',','.') }}</span></th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Thành tiền:</th>
                                        <?php $priceFinal = @$data['price_total'] ? $data['price_total'] - $priceDiscount : 0; ?>
                                        <th><span class="price-final text-danger">{{ number_format($priceFinal,0,',','.')  }}</span></th>
                                    </tr>
                                    </tfoot>
                                </table>

                                <div class="text-right">
                                    <button type="button" class="btn btn-primary" onclick="repeater(event,this,'{{route('repeat.order.product')}}','.index-product')">Thêm</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div> <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

        <div class="box box-solid">
            <div class="box-body">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
                @include("admin.components.save-as")
            </div>
        </div>
    </form>
@endsection

<?php $id = isset($id) ? $id : (int)round(microtime(true) * 1000); ?>
<tr>
    <td><span class="index-product">{{request('index') ?? $index}}</span></td>
    <td>
        <select name="order_detail[{{$id}}][product_id]" onchange="orderQuantityOnchange(this)" class="form-control select" style="width: 100%;">
            <option  data-price-in="0"
                     data-price-out="0" value="0">Chọn</option>
            @foreach($products as $product)
                <option value="{{$product->id}}"
                        data-price-in="{{$product->price_in}}"
                        data-price-out="{{$product->price_out}}"
                        {{ $product->id == @$val['product_id'] ? 'selected' : '' }}
                >{{$product->display_name}}</option>
            @endforeach
        </select>
    </td>
    <?php $priceTotal = @$val['price_total'] ? $val['price_total']/$val['quantity'] : 0; ?>
    <td><span class="price-out">{{ number_format($priceTotal ?? 0,0,',','.') }}</span></td>
    <td><input type="number" min="1" class="form-control quantity" onkeyup="orderQuantityOnchange(this)" onclick="orderQuantityOnchange(this)" name="order_detail[{{$id}}][quantity]" value="{{ $val['quantity'] ?? 1 }}"></td>
    <td style="position:relative">
        <span class="price-total">{{ number_format($val['price_total'] ?? 0,0,',','.')  }}</span>
        <a href="javascript:void(0);" onclick=" orderQuantityOnchange(this, true); $(this).closest('tr').remove();" class="repeat-btn-remove text-danger"
           title="Xóa"><i class="fa fa-minus"></i></a>
        <a href="javascript:void(0);" onclick="rowAdd(event,this,'{{route('repeat.order.product')}}','.index-product')"
           class="repeat-btn-add text-primary" title="Thêm"><i class="fa fa-plus"></i></a>
    </td>
</tr>

<script>
    $('.select').select2({
        placeholder: "Chọn",
    });
</script>

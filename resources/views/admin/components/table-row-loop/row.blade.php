@if(view()->exists("admin.components.table-row-loop.{$column}"))
    @include("admin.components.table-row-loop.{$column}")
@else
    <td>{{ $item->$column }}</td>
@endif

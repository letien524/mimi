<td>
    <a href="{{ route("{$module['model']}.edit",$item) }}" title="{{ $item->user->display_name ?? '' }}" class="text-default">
        <span class="name">{{ $item->user->display_name ?? '' }}</span>
    </a>
</td>

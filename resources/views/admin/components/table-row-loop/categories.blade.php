<td>
    @if(isCurrentRoute('product.index'))
        <?php $categories = $item->categories->implode('name',', ') ?>
        <a href="{{ route("{$module['model']}.edit",$item) }}" title="{{ $categories }}" class="text-default">
            {{$categories}}
        </a>
    @endif
</td>

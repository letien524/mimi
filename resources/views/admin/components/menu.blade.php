@if(isset($menu['children']))
    <li class="treeview {{ isCurrentModelSlug($model) ? 'active menu-open' : null }}">
        <a href="#">
            <i class="fa {{ $menu['icon'] }}"></i> <span>{{ $menu['name'] }}</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>

        <ul class="treeview-menu">
            @foreach($menu['children'] as $menu)
                <li class="{{ isCurrentModelSlug($menu['route'],true)  ? 'active' : null }}">
                    <a href="{{ route($menu['route']) }}"><i class="fa fa-circle-o"></i> {{ $menu['name'] }}</a>
                </li>
            @endforeach
        </ul>
    </li>
@else
    <li class="{{ isCurrentModelSlug($model) ? 'active' : null }}">
        <a href="{{ route($menu['route']) }}">
            <i class="fa {{ $menu['icon'] }}"></i> <span>{{ $menu['name'] }}</span>
        </a>
    </li>
@endif
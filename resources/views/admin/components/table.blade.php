<div class="table-responsive">
    <table class="table table-hover data-table">
        <thead>
        <tr>
            <th style="width: 30px;">
                <input type="checkbox" name="selectAll" onchange="inputSelectAll(this)">
            </th>
            <th style="width: 30px;">ID</th>
            @foreach(@$module['table'] as $column => $name)
                <th>{!! $name !!}</th>
            @endforeach
            @if(isAdminUrl())
                <th style="width: 150px;text-align: center;">Thao tác</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach(@$data as $item)
            <tr>
                <td><input type="checkbox" name="selectItem[]" value="{{ $item->id }}"></td>
                <td>{{ $item->id }}</td>
                @foreach($module['table'] as $column => $name)
                    @include("admin.components.table-row-loop.row")
                @endforeach
                @if(isAdminUrl())
                    <td class="text-center">
                        <a href="{{ route($module['model'].".edit",$item) }}" title="Sửa" class="btn btn-sm btn-primary"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route($module['model'].".destroy",$item) }}"
                           onclick="itemDeleteConfirm(this,event)"
                           data-target="#detroyConfirm" data-toggle="modal" class="btn btn-sm btn-danger" title="Xóa"><i class="fa fa-trash-o fa-fw"></i></a>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


@if(isUpdate(@$module['action']) && isDeveloper())
    <button type="button" data-action="{{ route($module['model'].".store") }}"
            onclick="modelSaveAs(this)" class="btn btn-success"><i class="fa fa-copy"></i> Lưu mới
    </button>
@endif
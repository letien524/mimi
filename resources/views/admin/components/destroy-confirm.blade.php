<div class="modal fade" id="detroyConfirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p class="text-danger">Bạn có đồng ý xóa đi không?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="post">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Xóa</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Hủy
                    </button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@if(URL::current() != url('backend'))
    <section class="content-header">
        <h1>
            <a href="@yield('module.index')">@yield('module.name')</a>
            <small>@yield('module.page')</small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{!! url('backend') !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="@yield('module.index')">@yield('module.name')</a></li>
            <li class="active">@yield('module.page')</li>
        </ol>
    </section>
@endif
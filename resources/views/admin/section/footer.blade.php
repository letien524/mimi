<footer class="main-footer">
    <strong>Copyright &copy; {{ \Carbon\Carbon::now()->format('Y') }}. <a href="mailto:letien524@gmail.com">letien524@gmail.com</a>
    </strong>
    All rights reserved.
</footer>
@if(session('flash_message'))
    <script>toastr["{{ session('flash_level') }}"]("{{  session('flash_message') }}")</script>
@endif

@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script>toastr["error"]("{{ $error }}")</script>
    @endforeach
@endif

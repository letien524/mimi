<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link href="{{ asset(config('admin.style.bootstrap')) }}" rel="stylesheet">
    <link href="{{ asset(config('admin.style.main-style')) }}" rel="stylesheet">
    <link href="{{ asset(config('admin.style.toastr')) }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- My style -->
    <link rel="stylesheet" href="{{ asset(config('admin.style.customize')) }}">

    <script src="{{ asset(config('admin.script.jquery')) }}"></script>
    <script src="{{ asset(config('admin.script.bootstrap')) }}"></script>
    <script src="{{ asset(config('admin.script.toastr')) }}"></script>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:void(0)">&#10053; <b>Login</b> &#10053;</a>
    </div>

    <div class="login-box-body">
        <div class="loginImg">
            <i class="fa fa-user-secret"></i>
        </div>
        <p class="login-box-msg">&#9824; Sign in to start your session &#9824;</p>

        @include('admin.section.notify')

        <form action="{{ route('login') }}" method="post" >
            {{ csrf_field() }}

            <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
                <input  placeholder="Username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input placeholder="Password" type="password" class="form-control" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id = "cloud"><span class='shadow'></span></div>

</body>
</html>

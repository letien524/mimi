<!DOCTYPE html>
<html lang="{!! config('app.locale') !!}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{imageUrlRender(@$site_settings['favicon'])}}" type="image/x-icon">

    {!! SEO::generate() !!}

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset(config('admin.style.toastr'))}}">

    <link rel="stylesheet" type="text/css" href="{{getAsset('css/font.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/common.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/gnb.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/main.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/sub.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/location.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/jquery-ui.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/jquery.selectbox.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{getAsset('bbs/css/editor.cs')}}s"/>
    <link rel="stylesheet" type="text/css" href="{{getAsset('css/extends.css')}}" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/font-nanum/1.0/nanumbarungothic/nanumbarungothic.css" />

    @yield('begin')


</head>
<body class="english">
@yield('script_head')
{!! @$site_settings['script_head'] !!}

@yield('content')


{!! @$site_settings['script_foot'] !!}
@yield('script_foot')
</body>

<script type="text/javascript" src="{{getAsset('js/jquery-1.11.js')}}"></script>
<script type="text/javascript" src="{{asset(config('admin.script.toastr'))}}"></script>
<script>
    $(function(){
        $('body').css('min-height',$('.gnb.gnb_serise nav ul:first-child').height() + 80)
    })
</script>
<script type="text/javascript" src="{{getAsset('js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{getAsset('js/bootstrap.js')}}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.0/jquery.flexslider.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.js"></script>
<script type="text/javascript" src="{{getAsset('js/custom.js')}}"></script>
<script type="text/javascript" src="{{getAsset('js/jquery.selectbox-0.2.js')}}"></script>
<script type="text/javascript" src="{{getAsset('js/jquery.isotope.js')}}"></script>
<script type="text/javascript" src="{{getAsset('bbs/js/editor_loader.js')}}"></script>
<script type="text/javascript" src="{{getAsset('imagesloaded/imagesloaded.js')}}"></script>
<script type="text/javascript" src="{{getAsset('js/oasis_popup.js')}}"></script>

@yield('end')
@include('admin.section.notify')
</html>
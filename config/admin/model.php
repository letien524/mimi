<?php
    $except = ['show'];
    return [
//        'dashboard' => [
//            'name' => 'dashboard',
//            'except' => $except
//        ],

        'user' => [
            'name' => 'user',
            'except' => $except
        ],

        'role' => [
            'name' => 'role',
            'except' => $except
        ],

        'order' => [
            'name' => 'order',
            'except' => $except
        ],

        'product' => [
            'name' => 'product',
            'except' => $except
        ],
    ];

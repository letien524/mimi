<?php
    return [
        'jquery' => 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
        'select2' => 'assets/admin/plugins/select2/select2.full.min.js',
        'bootstrap' => 'assets/admin/plugins/bootstrap/js/bootstrap.min.js',
        'fastclick' => 'assets/admin/plugins/fastclick/fastclick.js',
        'datatable' => 'assets/admin/plugins/datatables/jquery.dataTables.min.js',
        'datatable-bootstrap' => 'assets/admin/plugins/datatables/dataTables.bootstrap.min.js',
        'ckeditor' => 'assets/admin/plugins/ckeditor/ckeditor.js',
        'ckfinder' => 'assets/admin/plugins/ckfinder/ckfinder.js',
        'toastr' => 'assets/admin/plugins/toastr/toastr.min.js',
        'nestable' => 'assets/admin/plugins/nestable/jquery.nestable.js',
        /*'nestable-script' => 'assets/admin/plugins/nestable/script.js',*/
        'datepicker' => 'assets/admin/plugins/datepicker/bootstrap-datepicker.js',
        'main-script' => 'assets/admin/js/adminlte.js',
        'fontawesome-iconpicker' => 'assets/admin/plugins/fontawesome-iconpicker/js/fontawesome-iconpicker.min.js',
        'customize' => 'assets/admin/customize/script.js',
    ];

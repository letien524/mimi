<?php
    return [

        'user' => [
            'name' => 'Đại lý',
            'icon' => 'fa-users',
            'route' => 'user.index',
        ],

        'role' => [
            'name' => 'Cấp bậc',
            'icon' => 'fa-newspaper-o',
            'route' => 'role.index',
            'role' => 'admin',
        ],

        'order' => [
            'name' => 'Đơn hàng',
            'icon' => 'fa-newspaper-o',
            'route' => 'order.index',
        ],

        'product' => [
            'name' => 'Sản phẩm',
            'icon' => 'fa-newspaper-o',
            'route' => 'product.index',
            'role' => 'admin',

        ],
//
//        'blog' => [
//            'name' => 'Tin tức',
//            'icon' => 'fa-newspaper-o',
//            'children' => [
//                ['name' => 'Danh sách', 'route' => 'blog.index'],
//                ['name' => 'Cấu hình', 'route' => 'blogSetting.index'],
//            ]
//        ],
//

//
//        'about' => [
//            'name' => 'Giới thiệu',
//            'icon' => 'fa-newspaper-o',
//            'route' => 'about.index',
//        ],
//
//        'contact' => [
//            'name' => 'Liên hệ',
//            'icon' => 'fa-newspaper-o',
//            'children' => [
//                ['name' => 'Danh sách liên hệ',  'route' => 'contact.index',],
//                ['name' => 'Cấu hình', 'route' => 'contactSetting.index'],
//            ]
//        ],
//
//        'setting' => [
//            'name' => 'Cấu hình',
//            'icon' => 'fa-cogs',
//            'children' => [
//                ['name' => 'Cấu hình chung', 'route' => 'settingGeneral.index'],
//                ['name' => 'Mail SMTP', 'route' => 'settingMail.index'],
//            ]
//        ],

    ];

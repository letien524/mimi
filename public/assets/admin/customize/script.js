CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';

    config.toolbarGroups = [
        {name: 'document', groups: ['mode', 'document', 'doctools']},
        {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
        {name: 'forms', groups: ['forms']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
        {name: 'links', groups: ['links']},
        {name: 'insert', groups: ['insert']},
        {name: 'styles', groups: ['styles']},
        {name: 'colors', groups: ['colors']},
        {name: 'tools', groups: ['tools']},
        {name: 'others', groups: ['others']},
        {name: 'about', groups: ['about']}
    ];
    config.removeButtons = 'Styles,Font,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Replace,Find,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,Blockquote,Language,BidiRtl,BidiLtr,Anchor,Flash,Smiley,About';
};



$(function () {


    $('textarea.content').each(function () {
        var editor = CKEDITOR.replace($(this).attr('name'));
        CKFinder.setupCKEditor(editor);

    });

    function init() {
        var imgDefer = document.querySelectorAll('img.lazy');
        for (var i=0; i<imgDefer.length; i++) {
            if(imgDefer[i].getAttribute('data-src')) {
                imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
                imgDefer[i].setAttribute('srcset',imgDefer[i].getAttribute('data-src'));
            } } }
    window.onload = init;


    $(".data-table").DataTable({
        'columnDefs': [{

            'targets': 0, /* column index */

            'orderable': false, /* true or false */

        }],
        language: {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
    }).on( 'draw', function () {init();} );

    //Date picker
    $('.datepicker').datepicker({
        autoclose: true
    })



    $('input.name').keyup(function (event) {
        var title, slug;


        title = $(this).val();

        slug = title.toLowerCase();

        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        slug = slug.replace(/ /gi, "-");
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');

        slug = slug.replace(/\[|\]|\{|\}|\\/gi, '');

        // $('input#slug').val(slug);
        $($(this).data('target')).val(slug);
    });

    $('a#del_img').on('click', function () {
        var url = homeUrl() + "/backend/product/delimg/";
        var _token = $("form[name='frmEditProduct']").find("input[name='_token']").val();
        var idImg = $(this).parent().find("img").attr("idImg");
        var img = $(this).parent().find("img").attr("src");
        var rid = $(this).parent().find("img").attr("id");

        $.ajax({
            url: url + idImg,
            type: 'GET',
            cache: false,
            data: {"_token": _token, "idImg": idImg, "urlImg": img},
            success: function (data) {
                if (data == 'OK') {
                    $('#' + rid).remove();
                } else {
                    alert('Error ! Please contact admin !');
                }
            }
        });
    });


    $('.select').select2({
        placeholder: "Chọn",
    });

    var id = $('.liColor').attr('id');
    $("#iphiColor").val(id);

    $('.liColor').on('click', function (ev) {
        ev.preventDefault();
        $('.liColor').removeClass('active');
        $(this).addClass('active');

        var id = $(this).attr('id');
        $("#iphiColor").val(id);
    });


});


function fileSelect(el) {
    CKFinder.modal({
        chooseFiles: true,
        width: 1000,
        height: 500,
        onInit: function (finder) {

            finder.on('files:choose', function (evt) {
                var parent = $(el).closest('.image');
                var img = parent.find('img').first();
                var input = parent.find('input').first();

                var file = evt.data.files.first();
                var url = file.getUrl();
                img.attr('src', url);
                input.val(url);

            });

            finder.on('file:choose:resizedImage', function (evt) {

                var parent = $(el).closest('.image');
                var img = parent.find('img').first();
                var input = parent.find('input').first();

                var url = evt.data.resizedUrl;

                img.attr('src', url);

                input.val(url);
            });
        }
    });
}

function fileMultiSelect(el, name='gallery[]') {
    CKFinder.modal({
        chooseFiles: true,
        width: 1000,
        height: 500,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var parent = $(el).closest('.image');
                var gallery = parent.find('.image__gallery');
                var files = evt.data.files;

                files.forEach(function (file) {
                    var url = file.getUrl();

                    var result = '<div class="image__thumbnail image__thumbnail--style-1">' +
                        '<img src="' + url + '" >' +
                        '<a href="javascript:void(0)" class="image__delete" onclick="urlFileMultiDelete(this)"><i class="fa fa-times"></i></a>' +
                        '<input type="hidden" name="'+ name +'" value="' + url + '">' +
                        '</div>';

                    gallery.append(result)
                })


            });

            finder.on('file:choose:resizedImage', function (evt) {

                var parent = $(el).closest('.image');
                var gallery = parent.find('.image__gallery');

                var url = evt.data.resizedUrl;

                var result = '<div class="image__thumbnail image__thumbnail--style-1">' +
                    '<img src="' + url + '" >' +
                    '<a href="javascript:void(0)" class="image__delete" onclick="urlFileMultiDelete(this)"><i class="fa fa-times"></i></a>' +
                    '<input type="hidden" name="gallery[]" value="' + url + '">' +
                    '</div>';

                gallery.append(result)
            });
        }
    });


}

function urlFileDelete(el) {
    var parent = $(el).closest('.image');
    var img = parent.find('img').first();
    var input = parent.find('input').first();

    img.attr('src', img.data('init'));
    input.val('');
}

function urlFileMultiDelete(el) {
    $(el).closest('.image__thumbnail').remove();
}

function inputSelectAll(el) {
    el = $(el);
    var table = el.closest('table');
    var selectItem = table.find('input[name="selectItem[]"]');

    selectItem.prop('checked', false);
    if (el.is(':checked')) {
        selectItem.prop('checked', true);
    }
}

function itemDeleteConfirm(el) {
    el = $(el);
    var url = el.attr('href');
    var form = $(el.data('target')).find('form').first();
    form.attr('action', url);
    return false;
}

function modelSaveAs(el) {
    var form = $(el).closest('form');
    form.attr('action', $(el).data('action'));
    form.find('input[name=_method]').remove();
    return form.submit();
}



function rowAdd(event, el, url, indexClass) {
    event.preventDefault();
    var indexs = $(indexClass).closest('table').find(indexClass);
    var index = indexs.length;

    var target = $(el).closest('tr');
    $.get(url, {index: index + 1}, function (data) {
        target.after(data);
    });
}

function repeater(event, el, url, indexClass) {
    event.preventDefault();
    var target = $(el).closest('.repeater').find('table tbody');
    var indexs = $(indexClass).closest('table').find(indexClass);
    var index = indexs.length;

    $.get(url, {index: index + 1}, function (data) {
        target.append(data)
    })
}

function orderQuantityOnchange(el, del=false, isDiscount=false){
    el = $(el);
    let priceTotal = 0;
    let priceDiscount = 0;
    let priceFinal = 0;
    let discount = $("#discount").val();

    let table = $(isDiscount);

    if(isDiscount === false){
        table = el.closest('table');
        const parent = el.closest('tr');
        const selectedOption = parent.find('option:selected')[0];
        let priceIn = $(selectedOption).data('price-in');
        let priceOut = $(selectedOption).data('price-out');
        let quantity = $(parent.find('.quantity')).val();

        quantity = quantity.length > 0 ? parseInt(quantity) : 0;

        parent.find('.price-out').text(new Intl.NumberFormat('vi-VN', {}).format(priceOut));
        parent.find('.price-total').text(new Intl.NumberFormat('vi-VN', {}).format(quantity*priceOut));

        if(del){
            priceTotal -= quantity*priceOut;
        }
    }

    const priceGlobal = table.find('.price-global');
    const priceFinalEl = table.find('.price-final');
    const priceDiscountEl = table.find('.price-discount');
    table.find('tbody tr').each(function(){
        const parent = $(this);
        const selectedOption = parent.find('option:selected')[0];
        let priceIn = $(selectedOption).data('price-in');
        let priceOut = $(selectedOption).data('price-out');
        let quantity = $(parent.find('.quantity')).val();

        quantity = quantity.length > 0 ? parseInt(quantity) : 0;

        priceTotal += quantity*priceOut;

    })

    discount = discount.length > 0 ? parseInt(discount) : 0;

    priceDiscount = priceTotal*(discount/100);
    priceFinal = priceTotal - priceDiscount;

    priceFinal = priceFinal < 0 ? 0 : priceFinal;

    priceGlobal.text(new Intl.NumberFormat('vi-VN', {}).format(priceTotal));
    priceDiscountEl.text(new Intl.NumberFormat('vi-VN', {}).format(priceDiscount));
    priceFinalEl.text(new Intl.NumberFormat('vi-VN', {}).format(priceFinal));

}


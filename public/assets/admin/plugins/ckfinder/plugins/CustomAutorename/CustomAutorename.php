<?php

    namespace CKSource\CKFinder\Plugin\CustomAutorename;

    use CKSource\CKFinder\CKFinder;
    use CKSource\CKFinder\Event\BeforeCommandEvent;
    use CKSource\CKFinder\Event\CKFinderEvent;
    use CKSource\CKFinder\Filesystem\Folder\WorkingFolder;
    use CKSource\CKFinder\Plugin\PluginInterface;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use Symfony\Component\HttpFoundation\File\UploadedFile;

    class CustomAutorename implements PluginInterface, EventSubscriberInterface
    {
        protected $app;

        public function setContainer(CKFinder $app)
        {
            $this->app = $app;
        }

        public function getDefaultConfig()
        {
            return [];
        }

        public function onBeforeUpload(BeforeCommandEvent $event)
        {
            $request = $event->getRequest();

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $request->files->get('upload');

            /** @var WorkingFolder $workingFolder */
            $workingFolder = $this->app['working_folder'];


            if ($uploadedFile) {

                // And here's the hack to make a private property accessible to set a new file name.
                $setOriginalName = function (UploadedFile $file, $newFileName) {
                    $file->originalName = $newFileName;
                };
                $setOriginalName = \Closure::bind($setOriginalName, null, $uploadedFile);

                $uploadedFileName = $uploadedFile->getClientOriginalName();

                $uploadedFileName = $this->stripVN($uploadedFileName);

                if (!$workingFolder->containsFile($uploadedFileName)) {
                    // File with this name doesn't exist, nothing to do here.
                    $setOriginalName($uploadedFile, $uploadedFileName);
                    return;
                }

                $basename = pathinfo($uploadedFileName, PATHINFO_FILENAME);
                $extension = pathinfo($uploadedFileName, PATHINFO_EXTENSION);

                $i = 0;

                // Increment the counter until there's no file named like this in current folder.
                while (true) {
                    $i++;
                    $uploadedFileName = "{$basename}-{$i}.{$extension}";

                    if (!$workingFolder->containsFile($uploadedFileName)) {
                        break;
                    }
                }


                $setOriginalName($uploadedFile, $uploadedFileName);
            }
        }


        public static function getSubscribedEvents()
        {
            return [CKFinderEvent::BEFORE_COMMAND_FILE_UPLOAD => 'onBeforeUpload'];
        }

        function stripVN($str) {
            $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
            $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
            $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
            $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
            $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
            $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
            $str = preg_replace("/(đ)/", 'd', $str);

            $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
            $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
            $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
            $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
            $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
            $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
            $str = preg_replace("/(Đ)/", 'D', $str);

            $str = preg_replace('/[^a-zA-Z0-9\'^.]/', '-', $str);


            // trim
            $str = trim($str, '-');

            // remove duplicate -
            $str = preg_replace('~-+~', '-', $str);


            return $str;
        }
    }
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name',191)->unique();
            $table->string('image',255)->nullable();
            $table->string('password',255);
            $table->bigInteger('role_id',false, true)->nullable();
            $table->bigInteger('parent_id', false, true)->nullable();

            $table->string('display_name',255)->nullable();
            $table->string('department_name',255)->nullable();
            $table->string('email',255)->nullable();
            $table->string('phone',255)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('province', 255)->nullable();
            $table->string('type', 255)->nullable();

            $table->string('bank_account_name', 255)->nullable();
            $table->string('bank_account_number', 255)->nullable();
            $table->string('facebook', 255)->nullable();
            $table->text('note')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parent_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

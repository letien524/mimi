<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        $data = [
            [
                'name' => 'superadmin',
                'password' => '$2y$10$.MKAgQw4XCAqnWBGB3ib8O5QsdPPcDTgpotyiap71WN0hvBvoQdQq',
                'display_name' => 'superadmin',
                'role_id' => 1,
                'parent_id' => NULL,
                'department_name' => NULL,
                'email' => NULL,
                'phone' => NULL,
                'date_of_birth' => NULL,
                'province' => NULL,
                'type' => NULL,
                'bank_account_name' => NULL,
                'bank_account_number' => NULL,
                'facebook' => NULL,
                'note' => NULL,
            ],
            [
                'name' => 'admin_phuong',
                'password' => '$2y$10$hQ9FZ5NjErYKO/qKSeb2B.WfOdtSyKIs.wd5PYxrZZT/6uj2xb.YO',
                'display_name' => 'Admin Phuong',
                'role_id' => 2,
                'parent_id' => NULL,
                'department_name' => NULL,
                'email' => NULL,
                'phone' => NULL,
                'date_of_birth' => NULL,
                'province' => NULL,
                'type' => NULL,
                'bank_account_name' => NULL,
                'bank_account_number' => NULL,
                'facebook' => NULL,
                'note' => NULL,
            ],
        ];

        $faker = \Faker\Factory::create();
        for ($i = 2; $i < 50; $i++) {
            $data[$i]['name'] = "demo" . ($i + 1);
            $data[$i]['password'] = \Illuminate\Support\Facades\Hash::make(1212);
            $data[$i]['display_name'] = $faker->name;

            $data[$i]['role_id'] = \DB::table('roles')->whereNotIn('name', ['root'])->inRandomOrder()->first()->id;
            $data[$i]['parent_id'] = $faker->numberBetween(1, $i);
            $data[$i]['department_name'] = $faker->company;
            $data[$i]['email'] = $faker->email;
            $data[$i]['phone'] = $faker->phoneNumber;
            $data[$i]['date_of_birth'] = $faker->date();
            $data[$i]['province'] = $faker->streetAddress;
            $data[$i]['type'] = $faker->name;
            $data[$i]['bank_account_name'] = $faker->company;
            $data[$i]['bank_account_number'] = $faker->bankAccountNumber;
            $data[$i]['facebook'] = $faker->url;
            $data[$i]['note'] = $faker->text;
        }

        \DB::table('users')->insert($data);
    }
}

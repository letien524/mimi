<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('roles')->delete();

        $data = [
            ['name' => 'root', 'display_name' => 'Root'],
            ['name' => 'quan-tri-he-thong', 'display_name' => 'Quản trị hệ thống'],
        ];

        $faker = \Faker\Factory::create();
        for ($i = 2; $i < 50; $i++) {
            $name = $faker->name;
            $data[$i]['name'] = $name;
            $data[$i]['display_name'] = $name;
        }

        \DB::table('roles')->insert($data);
    }
}

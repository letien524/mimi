<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*php artisan iseed users,password_resets,blogs,pages,settings,contacts,products,product_categories,category_product --force*/
//        DB::table('users')->insert([
//            ['name'=>'superadmin', 'display_name' => 'superadmin', 'password' => '$2y$10$.MKAgQw4XCAqnWBGB3ib8O5QsdPPcDTgpotyiap71WN0hvBvoQdQq', 'parent_id' => null],
//            ['name'=>'admin_phuong', 'display_name' => 'gco_admin', 'password' => bcrypt('1a2s3d4f5g6h'), 'parent_id'=> 1],
//            ['name'=>'gco_admin1', 'display_name' => 'gco_admin1', 'password' => bcrypt('1a2s3d4f5g6h'), 'parent_id'=> null],
//            ['name'=>'gco_admin2', 'display_name' => 'gco_admin2', 'password' => bcrypt('1a2s3d4f5g6h'), 'parent_id'=> 3],
//        ]);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrderDetailsTableSeeder::class);
    }
}

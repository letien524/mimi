<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('orders')->delete();
        $faker = \Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 100; $i++) {
            $data[$i]['user_id'] = \DB::table('users')->whereNotIn('id',[1])->inRandomOrder()->first()->id;
            $data[$i]['discount'] = $faker->numberBetween(0, 100);
            $data[$i]['price_total'] = $faker->numberBetween(100, 100000);
            $data[$i]['note'] = $faker->text;
            $data[$i]['created_at'] = $faker->dateTime();
            $data[$i]['updated_at'] = $faker->dateTime();
        }
        \DB::table('orders')->insert($data);
    }
}

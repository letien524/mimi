<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->delete();
        $faker = \Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 100; $i++) {
            $data[$i]['display_name'] = $faker->name;
            $data[$i]['price_in'] = $faker->numberBetween(10, 1000);
            $data[$i]['price_out'] = $faker->numberBetween(10, 1000);
        }
        \DB::table('products')->insert($data);
    }
}

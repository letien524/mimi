<?php

use Illuminate\Database\Seeder;

class OrderDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('order_details')->delete();
        $faker = \Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 1000; $i++) {
            $data[$i]['product_id'] = \DB::table('products')->inRandomOrder()->first()->id;
            $data[$i]['order_id'] = \DB::table('orders')->inRandomOrder()->first()->id;
            $data[$i]['quantity'] = $faker->numberBetween(10, 100);
            $data[$i]['price_total'] = $faker->numberBetween(100, 100000);
        }
        \DB::table('order_details')->insert($data);
    }
}

<?php

    use Artesaos\SEOTools\Facades\SEOMeta;
    use Artesaos\SEOTools\Facades\OpenGraph;

    function getAuthField($field){
        return @auth()->user()->{$field};
    }

    function getCurrentLang()
    {
        return app()->getLocale();
    }

    function isCurrentLang($lang){
        return getCurrentLang() == $lang;
    }

    function isCurrentRoute($route)
    {
        if (request()->route()->getName() == $route) {
            return true;
        }
    }

    function getSettings($name)
    {
        $settings = \App\Setting::firstOrCreate(['name' => $name]);
        return $settings['content'];
    }

    function getProductCategories()
    {
        $data = \App\ProductCategory::isDisplay()->where('parent_id',null)->orWhere('parent_id',0)->latest('order_menu')->get();
        $data->load(['products','children'=>function($query){
            $query->whereHas('products',function($query){
                $query->where('display',1)->where('product_id','>',0);
            });
        }]);


        return $data->count()? $data : collect([]);
    }

    function seoTitle($content = null)
    {
        $settings = getSettings('general');
        $content = strlen($content) ? $content : $settings[getColumnByLang('meta_title')];

        SEOMeta::setTitle($content);
        OpenGraph::setTitle($content);
    }

    function seoDescription($content = null)
    {
        $settings = getSettings('general');
        $content = strlen($content) ? $content : $settings[getColumnByLang('meta_description')];
        SEOMeta::setDescription($content);
        OpenGraph::setDescription($content);
    }

    function seoImage($content = null)
    {
        $settings = getSettings('general');

        $content = strlen($content) ? $content : $settings['meta_image'];

        OpenGraph::addImage(imageUrlRender($content), ['secure_url' => imageUrlRender($content)]);
    }

    function getSiteRobots($robots)
    {
        if ($robots) {
            return 'index,follow';
        }
        return 'noindex,nofollow';
    }

    function getAsset($path, $prefix = 'assets/kpmtech/')
    {
        return asset($prefix . $path);
    }

    function getColumnByLang($field, $defaultLang = 'vi')
    {
        $currentLang = app()->getLocale();

        if ($currentLang != $defaultLang) {
            $field = "{$field}_{$currentLang}";
        }

        return $field;
    }

    function isCurrentModelSlug($args, $children = false)
    {
        $currentModelSlug = request()->segment(2);
        if ($children) {
            $args = explode('.', $args);
            return in_array($currentModelSlug, $args);
        }
        return is_numeric(strpos($currentModelSlug, $args));
    }

    function isUpdate($method)
    {
        return (bool)$method == 'update';
    }

    function updateOrStoreRouteRender($method, $model, $data)
    {
        return isUpdate($method) ? route($model . '.update', $data) : route($model . '.store');
    }

    function imageUrlRender($path, $prefix = null)
    {
        return url($prefix . (strlen($path) ? $path : __IMAGE_THUMBNAIL_DEFAULT));
    }

    function getValueField($name, $data)
    {
        $name = explode('.', $name);
        $result = $data[$name[0]];

        if (count($name) > 1) {

            $result = !is_array($result) ? json_decode($result, true) : $result;


            for ($i = 1; $i < count($name); $i++) {
                $result = $result[$name[$i]];

            }

        }
        return $result;
    }

    function getNameField($name)
    {

        $name = explode('.', $name);

        if (count($name) > 1) {
            for ($i = 1; $i < count($name); $i++) {
                $name[$i] = "[{$name[$i]}]";
            }
            return implode('', $name);
        }
        return $name[0];
    }

    function isDeveloper()
    {

        if (getAuthField('level') == 1 && getAuthField('name') == 'superadmin') {
            return true;
        }
        return false;
    }

    function isAdminUrl(){
        return in_array(request()->segment(2), ['order','user']) && isAdmin() ||  in_array(request()->segment(3), ['create']);
    }

    function isAdmin()
    {
        return in_array(auth()->user()->role->name, ['root','quan-tri-he-thong']);
    }


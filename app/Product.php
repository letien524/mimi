<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['display_name','price_in','price_out'];

    public function orderDetail(){
        return $this->hasMany(OrderDetail::class);
    }

    public function setPriceInAttribute($value)
    {
        $this->attributes['price_in'] = (int) $value;
    }

    public function setPriceOutAttribute($value)
    {
        $this->attributes['price_out'] = (int) $value;
    }
}

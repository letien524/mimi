<?php

namespace App\Http\Controllers;

use App\Http\Middleware\isDeveloper;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('adminRole')->only(['destroy','bulkDestroy','update']);
    }

    protected function fields()
    {
        /*^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$*/
        return [
            'name' => "required|unique:users,name|regex:/^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/",
            'password' => "required",
        ];
    }

    protected function messages()
    {
        return [];
    }

    protected $model = 'user';

    protected function module()
    {
        return [
            'name' => 'Đại lý',
            'model' => 'user',
            'table' => [
                'display_name' => 'Đại lý',
                'phone' => 'Số điện thoại',
                'role_id' => 'Cấp bậc',
                'parent_id' => 'Cấp trên',
                'children_count' => 'Số lượng con',
            ]
        ];
    }

    public function index()
    {
        $data['data'] = User::whereNotIn('name',['superadmin'])
            ->filter()
            ->latest('id')->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        $data['users'] = User::orderBy('display_name')->get(['id','display_name']);
        $data['roles'] = Role::whereNotIn('name',['root'])->filterRole()->orderBy('display_name')->get(['id','display_name','name']);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $input['password'] = bcrypt($input['password']);

        $data = User::create($input);

        return redirect()->route("{$this->module()['model']}.edit", $data)->with($this->flashMessages);

    }

    public function edit($id)
    {
        $user = User::filter()->findOrFail($id);
        $user['password'] = '';
        $data['data'] = $user;
        $data['module'] = array_merge($this->module(), [
            'action' => 'update'
        ]);
        $data['users'] = User::whereNotIn('id', [$user->id])->whereNotIn('name', ['superadmin'])->filterRole()->orderBy('display_name')->get(['id', 'display_name','name']);
        $data['roles'] = Role::whereNotIn('name',['root'])->filterRole()->orderBy('display_name')->get(['id','display_name']);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, $id)
    {
        $user = User::filter()->findOrFail($id);
        $input = $request->all();
        $input['password']  = strlen($input['password']) ? bcrypt($input['password']) : $user->password;
        $user->update($input);

        return back()->with($this->flashMessages);
    }
}

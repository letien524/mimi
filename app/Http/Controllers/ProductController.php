<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;

class ProductController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('adminRole');
    }

    protected function fields()
    {

        return [];
    }

    protected function messages()
    {
        return [];
    }

    protected $model = 'product';

    protected function module()
    {
        return [
            'name' => 'Sản phẩm',
            'model' => 'product',
            'table' => [
                'display_name' => 'Tiêu đề',
                'price_in' => 'Giá nhập kho',
                'price_out' => 'Giá bán',
            ]
        ];
    }

    public function index()
    {

        $data['data'] = Product::latest('id')->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();
        $data = Product::create($input);

        return redirect()->route("{$this->module()['model']}.edit", $data)->with($this->flashMessages);

    }

    public function edit(Product $product)
    {


        $data['data'] = $product;
        $data['module'] = array_merge($this->module(), [
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, Product $product)
    {

        $input = $request->all();
        $product->update($input);

        return back()->with($this->flashMessages);
    }
}

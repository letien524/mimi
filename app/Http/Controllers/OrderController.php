<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OrderController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('adminRole')->only(['destroy','bulkDestroy','update']);
    }

    protected function fields()
    {

        return [
            'user_id'=>'required'
        ];
    }

    protected function messages()
    {
        return [
            'user_id.required' => 'Bạn chưa chọn Đại lý'
        ];
    }

    protected $model = 'order';

    protected function module()
    {
        return [
            'name' => 'Đơn hàng',
            'model' => 'order',
            'table' => [
                'user_id' => 'Đại lý',
                'price_total' => 'Tổng tiền',
                'discount' => 'Chiết khấu',
                'created_at' => 'Ngày tạo đơn',
            ]
        ];
    }

    public function index()
    {

        $data['data'] = Order::filter()->latest('id')->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        $data['users'] = User::whereNotIn('name', ['superadmin'])->orderBy('display_name')->get(['id', 'display_name']);
        $data['products'] = Product::orderBy('display_name')->get();
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }

    public function store(Request $request)
    {

        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();
        $input = $this->parseOrderDetail($input);

        $data = Order::create($input);
        $data->orderDetails()->createMany($input['order_detail']);

        return redirect()->route("{$this->module()['model']}.edit", $data)->with($this->flashMessages);

    }

    public function edit($id)
    {
        $order = Order::filter()->findOrFail($id);
        $data['data'] = $order;
        $data['users'] = User::whereNotIn('name', ['superadmin'])->orderBy('display_name')->get(['id', 'display_name']);
        $data['products'] = Product::orderBy('display_name')->get();
        $data['module'] = array_merge($this->module(), [
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, Order $order)
    {

        $input = $request->all();
        $input = $this->parseOrderDetail($input);

        $order->update($input);

        $order->orderDetails()->delete();
        $order->orderDetails()->createMany($input['order_detail']);

        return back()->with($this->flashMessages);
    }

    private function parseOrderDetail($input){
        $input['order_detail'] = isset($input['order_detail']) ? $input['order_detail'] : [];
        $input['order_detail'] = array_filter($input['order_detail'], function ($value){
            return $value['product_id'] > 0;
        });

        $idProducts = array_map(function($value){
            return $value['product_id'];
        }, $input['order_detail']);

        $products = Product::whereIn('id', $idProducts)->get();

        $input['order_detail'] = array_map(function($value) use($products){
            $product = $products->where('id', $value['product_id'])->first();
            $value['price_total'] = $product->price_out * $value['quantity'];
            return $value;
        }, $input['order_detail']);

        $input['price_total'] = array_sum(array_map(function($value){return $value['price_total'];},$input['order_detail']));
        return  $input;
    }
}

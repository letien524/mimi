<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoleController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('adminRole');
    }

    protected function fields()
    {

        return [
            'display_name' => 'required|unique:roles,display_name,'. request()->segment(3),
        ];
    }

    protected function messages()
    {
        return [];
    }

    protected $model = 'role';

    protected function module()
    {
        return [
            'name' => 'Cấp bậc',
            'model' => 'role',
            'table' => [
                'display_name' => 'Tiêu đề',
            ]
        ];
    }

    public function index()
    {

        $data['data'] = Role::whereNotIn('name',['root','quan-tri-he-thong'])->latest('id')->get(['id','display_name']);
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();
        $input['name'] = Str::slug($input['display_name']);
        $data = Role::create($input);

        return redirect()->route("{$this->module()['model']}.edit", $data)->with($this->flashMessages);

    }

    public function edit(Role $role)
    {


        $data['data'] = $role;
        $data['module'] = array_merge($this->module(), [
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, Role $role)
    {

        $input = $request->all();
        $input['name'] = Str::slug($input['display_name']);
        $role->update($input);

        return back()->with($this->flashMessages);
    }
}

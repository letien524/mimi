<?php

namespace App\Http\Controllers;

use App\UserGrade;
use Illuminate\Http\Request;

class UserGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserGrade  $userGrade
     * @return \Illuminate\Http\Response
     */
    public function show(UserGrade $userGrade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserGrade  $userGrade
     * @return \Illuminate\Http\Response
     */
    public function edit(UserGrade $userGrade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserGrade  $userGrade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserGrade $userGrade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserGrade  $userGrade
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserGrade $userGrade)
    {
        //
    }
}

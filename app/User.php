<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'image',
        'password',
        'parent_id',
        'display_name',
        'department_name',
        'email',
        'phone',
        'date_of_birth',
        'province',
        'type',
        'bank_account_name',
        'bank_account_number',
        'facebook',
        'note',
        'role_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function children()
    {
        return $this->hasMany(User::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

    public function allChildren()
    {
        $users = collect([]);
        $this->load('children');
        $children = $this->children;

        while ($children->count()) {
            $nextChildren = collect([]);
            $users = $users->merge($children);
            foreach ($children as $item) {
                $nextChildren = $nextChildren->merge($item->children);
            }
            $children = $nextChildren;
        }
        return $users;
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getDateOfBirthAttribute()
    {
        return Carbon::parse($this->date_of_birth)->format('m/d/Y');
    }

    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = Carbon::parse($value)->format('Y-m-d');;
    }

    public function scopeFilter($query)
    {

        if (!isAdmin()) {
            $user = auth()->user();
            $child = $user->allChildren();
            $child->push($user);
            $idChilds = $child->count() ? $child->pluck('id')->toArray() : [$user->id];
            return $query->whereIn('id', $idChilds);
        }
        return $query;
    }

    public function scopeFilterRole($query)
    {

//        if (!isAdmin()) {
//            return $query->whereIn('id', [auth()->user()->id]);
//        }
        return $query;
    }

}

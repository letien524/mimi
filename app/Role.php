<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','display_name'];

    public function users(){
        return $this->hasMany(User::class);
    }

    public function scopeFilterRole($query){
        if(!isAdmin()){
            return $query->whereNotIn('name',['quan-tri-he-thong']);
        }
        return $query;
    }

}

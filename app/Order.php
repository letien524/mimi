<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','discount','price_total','note'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function orderDetails(){
        return $this->hasMany(OrderDetail::class);
    }

    public function setDiscountAttribute($value)
    {
        $this->attributes['discount'] = (int) $value;
    }

    public function scopeFilter($query){
        $user = auth()->user();
        if (!isAdmin()) {
            $child = $user->allChildren();
            $child->push($user);
            $idChilds = $child->count() ? $child->pluck('id')->toArray() : [$user->id];
            return $query->whereIn('user_id', $idChilds);
        }
        return $query;
    }

}
